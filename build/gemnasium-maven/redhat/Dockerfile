ARG UBI_IMAGE=registry.access.redhat.com/ubi8/ubi-minimal:8.9
ARG UBI_IMAGE_MICRO=registry.access.redhat.com/ubi8/ubi-micro:8.9

FROM registry.gitlab.com/gitlab-org/gitlab-runner/go-fips:1.22.2 AS analyzer-builder
# Build a static binary
ENV CGO_ENABLED=0
WORKDIR /go/src/app
COPY go.mod go.sum ./
RUN go mod download

COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
    PATH_TO_MODULE=`go list -m` && \
    go version && \
    go build -ldflags="-X '$PATH_TO_MODULE/cmd/gemnasium-maven/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o analyzer \
    cmd/gemnasium-maven/main.go

FROM ${UBI_IMAGE_MICRO} as initial

FROM ${UBI_IMAGE} as build

ARG DNF_INSTALL_ROOT=/install-root
ARG DNF_OPTS_ROOT="--installroot=${DNF_INSTALL_ROOT}/ --setopt=reposdir=${DNF_INSTALL_ROOT}/etc/yum.repos.d/ --setopt=varsdir=/install-var/ --config=${DNF_INSTALL_ROOT}/etc/yum.repos.d/ubi.repo --setopt=cachedir=/install-cache/ --noplugins -y"

ENV VRANGE_DIR="${DNF_INSTALL_ROOT}/vrange"

RUN mkdir -p ${DNF_INSTALL_ROOT}/ /install-var
COPY --from=initial / ${DNF_INSTALL_ROOT}/ 

RUN microdnf ${DNF_OPTS_ROOT} update --nodocs && \
    microdnf ${DNF_OPTS_ROOT} install which git tar jq curl ca-certificates zstd unzip java-17-openjdk java-11-openjdk java-1.8.0-openjdk --nodocs && \
    microdnf ${DNF_OPTS_ROOT} clean all

COPY build/gemnasium-maven/utils/maven-plugin-builder ${DNF_INSTALL_ROOT}/maven-plugin-builder
COPY build/gemnasium-maven/gemnasium-init.gradle ${DNF_INSTALL_ROOT}/
COPY build/gemnasium-maven/utils/gradle-plugin-builder ${DNF_INSTALL_ROOT}/gradle-plugin-builder

COPY build/gemnasium-maven/utils/sbt-plugin-builder ${DNF_INSTALL_ROOT}/sbt-plugin-builder

ENV HOME=/gemnasium-maven
ENV VRANGE_DIR="/vrange"
WORKDIR ${DNF_INSTALL_ROOT}$HOME

COPY vrange/semver/vrange-linux ${DNF_INSTALL_ROOT}$VRANGE_DIR/semver/
COPY build/gemnasium-maven/redhat/install.sh ${DNF_INSTALL_ROOT}/root
COPY build/gemnasium-maven/redhat/config/.tool-versions ${DNF_INSTALL_ROOT}$HOME
COPY build/gemnasium-maven/redhat/config/.bashrc ${DNF_INSTALL_ROOT}$HOME

FROM ${UBI_IMAGE_MICRO}

ARG DNF_INSTALL_ROOT=/install-root

ENV VRANGE_DIR="/vrange"

ARG GEMNASIUM_DB_LOCAL_PATH="/gemnasium-db"
ARG GEMNASIUM_DB_REMOTE_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db.git"
ARG GEMNASIUM_DB_WEB_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db"
ARG GEMNASIUM_DB_REF_NAME="master"

ENV GEMNASIUM_DB_LOCAL_PATH $GEMNASIUM_DB_LOCAL_PATH
ENV GEMNASIUM_DB_REMOTE_URL $GEMNASIUM_DB_REMOTE_URL
ENV GEMNASIUM_DB_WEB_URL $GEMNASIUM_DB_WEB_URL
ENV GEMNASIUM_DB_REF_NAME $GEMNASIUM_DB_REF_NAME

ENV GRADLE_PLUGIN_INIT_PATH="/gemnasium-init.gradle"

# Disable features that are incompatible with FIPS mode
ENV FIPS_MODE="true"

ENV ASDF_DATA_DIR="/opt/asdf"
ENV ASDF_VERSION="v0.8.1"
ENV HOME=/gemnasium-maven
ENV TERM="xterm"

COPY --from=build  ${DNF_INSTALL_ROOT}/ /

WORKDIR $HOME

RUN cat /etc/*-release; \
        bash /root/install.sh

# Install analyzer
COPY --from=analyzer-builder --chown=root:root /go/src/app/analyzer /analyzer-binary
COPY build/gemnasium-maven/analyzer-wrapper /analyzer

CMD ["/analyzer", "run"]
