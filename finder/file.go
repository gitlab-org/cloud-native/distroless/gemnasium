package finder

// File is a dependency file that has been found.
type File struct {
	Filename string   // Filename is the name of the file
	FileType FileType // FileType is the type of file
}

// Scannable is true when the file contains a dependency list
// of a dependency graph that can be parsed and scanned
func (f File) Scannable() bool {
	switch f.FileType {
	case FileTypeLockFile, FileTypeGraphExport:
		return true
	default:
		return false
	}
}

// Linkable is true when it is possible to link to the file in the report
func (f File) Linkable() bool {
	switch f.FileType {
	case FileTypeGraphExport:
		return false
	default:
		return true
	}
}
