
# semver-parser

This example projects parses a bunch of JSON documents, to see if a given version satisfies semver constraints:

```json
{ "range": "^2.0.0", "version": "1.1.0" }
```

Precondition for both scripts is a successful `yarn install`

There are two versions of the script:

## stdio.js

This parses a stream of JSON documents separated by `\r\n` (similar to TCP messages) of the format above:

```json
{ "range": "^2.0.0", "version": "1.1.0" }
{ "range": "^2.0.0", "version": "2.1.0" }
```

It will also return a stream of result documents written to stdout.

Simply run:

```bash
cat example.jsonstream | ./stdio.js
# or
cat example.jsonstream | node ./stdio.js
# or 
./stdio.js <example.jsonstream
```

## rangecheck.js

This parses a plain old list of JSON documents which are a proper JSON array from a file:

```json
[
{ "range": "^2.0.0", "version": "1.1.0" },
{ "range": "^2.0.0", "version": "2.1.0" }
]
```

It will also return a JSON object to stdout.

Simply run:

```bash
./rangecheck.js example.json 
# or
node ./rangecheck.js example.json
```