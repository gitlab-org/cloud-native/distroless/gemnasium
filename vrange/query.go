package vrange

// Query combines a version and a range of versions.
// It is passed to the resolver to know if the version is in the range.
type Query struct {
	Range   string `json:"range"`   // Range is a range of versions
	Version string `json:"version"` // Version to be compared to Range
}

func (q Query) String() string {
	return q.Version + " ? " + q.Range
}
