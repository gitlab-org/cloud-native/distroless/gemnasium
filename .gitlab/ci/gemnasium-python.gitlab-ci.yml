variables:
  # Temporary image: registry.gitlab.com/gitlab-org/security-products/analyzers/gemnasium/tmp/python
  # Local image:     registry.gitlab.com/gitlab-org/security-products/analyzers/gemnasium/python
  # Official image:  registry.gitlab.com/security-products/gemnasium-python
  IMAGE_ALIAS: "python"
  ANALYZER: "gemnasium-python"

  SEC_REGISTRY_IMAGE:  "$GEMNASIUM_PYTHON_SEC_REGISTRY_IMAGE"
  SEC_REGISTRY_USER: "$GEMNASIUM_PYTHON_SEC_REGISTRY_USER"
  SEC_REGISTRY_PASSWORD: "$GEMNASIUM_PYTHON_SEC_REGISTRY_PASSWORD"

  MAX_IMAGE_SIZE_MB: 328
  MAX_IMAGE_SIZE_MB_FIPS: 1580

include:
  local: "/.gitlab/ci/image.gitlab-ci.yml"

.python 3.11:
  variables:
    DS_PYTHON_VERSION: "3.11"
    IMAGE_TAG_SUFFIX: "-python-$DS_PYTHON_VERSION"

build tmp image:
  variables:
    DOCKERFILE: "build/$ANALYZER/debian/Dockerfile"
  # override job script to pass extra build argument DS_PYTHON_VERSION
  script:
    - docker info
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build --build-arg DS_PYTHON_VERSION --build-arg GO_VERSION -t $TMP_IMAGE$IMAGE_TAG_SUFFIX -f $DOCKERFILE .
    - docker push $TMP_IMAGE$IMAGE_TAG_SUFFIX

build tmp image fips:
  variables:
    DOCKERFILE: "build/$ANALYZER/redhat/Dockerfile"

build tmp image 3.11:
  extends:
    - "build tmp image"
    - .python 3.11

image sbom 3.11:
  extends:
    - image sbom
    - .python 3.11

tag branch 3.11:
  extends:
    - "tag branch"
    - .python 3.11

release patch 3.11:
  extends:
    - "release patch"
    - .python 3.11

release major 3.11:
  extends:
    - "release major"
    - .python 3.11

release minor 3.11:
  extends:
    - "release minor"
    - .python 3.11

release latest 3.11:
  extends:
    - "release latest"
    - .python 3.11

image test:
  script:
    - rspec spec/gemnasium-python_image_spec.rb --tag "~python_version:${DS_PYTHON_VERSION/./}"

image test fips:
  script:
    - rspec spec/gemnasium-python_image_spec.rb --tag "~python_version:${DS_PYTHON_VERSION/./}"

image test 3.11:
  extends:
    - "image test"
    - .python 3.11
  script:
    - rspec spec/gemnasium-python_image_spec.rb --tag ~python_version:311

.functional:
  extends: .qa-downstream-ds
  variables:
    DS_EXCLUDED_ANALYZERS: "gemnasium,gemnasium-maven,bundler-audit,retire.js"
    DS_PYTHON_VERSION: "3.11"

# check to ensure that the Dependency-Scanning.gitlab-ci.yml template correctly saves gl-sbom-*.cdx.json files
python-pipenv-qa:
  extends: .functional
  variables:
    MAX_SCAN_DURATION_SECONDS: 22
    EXPECTATION: "python-pipenv/default"
    EXPECTED_CYCLONEDX_ARTIFACTS: "gl-sbom-pypi-pipenv.cdx.json"
  trigger:
    project: gitlab-org/security-products/tests/python-pipenv

python-pipenv-qa fips:
  extends:
    - .qa-fips
    - python-pipenv-qa

python-pipenv-offline-qa:
  extends: .functional
  variables:
    MAX_SCAN_DURATION_SECONDS: 19
    EXPECTATION: "python-pipenv/default"
  trigger:
    project: gitlab-org/security-products/tests/python-pipenv
    branch: offline-FREEZE

# Test integration with a custom build job that builds Python Wheels for default version of Python.
# The build job provides the system libraries required to build Pillow, but the scanning job doesn't.
python-pip-qa:
  extends: .functional
  variables:
    MAX_SCAN_DURATION_SECONDS: 19
    EXPECTATION: "python-pip/default"
  trigger:
    project: gitlab-org/security-products/tests/python-pip

python-pip-qa fips:
  extends:
    - .qa-fips
    - python-pip-qa

.functional 3.11:
  extends:
    - .qa-downstream-ds
    - .python 3.11

python-pipenv-offline-qa 3.11:
  extends:
    - python-pipenv-offline-qa
    - .python 3.11
