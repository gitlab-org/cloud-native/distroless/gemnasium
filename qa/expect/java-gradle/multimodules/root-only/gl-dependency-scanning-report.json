{
  "version": "15.0.7",
  "vulnerabilities": [
    {
      "id": "c0ac6dfffbbf9f9c5c1a053107f9e6a7b11eb1f6d968a1aee2f1a6fc93dbe452",
      "name": "Inconsistent Interpretation of HTTP Requests (HTTP Request Smuggling)",
      "description": "`HttpObjectDecoder.java` in Netty allows an HTTP header that lacks a colon, which might be interpreted as a separate header with an incorrect syntax, or might be interpreted as an invalid fold.",
      "severity": "Critical",
      "solution": "Upgrade to version 4.1.44.Final or above.",
      "location": {
        "file": "build.gradle",
        "dependency": {
          "package": {
            "name": "io.netty/netty-all"
          },
          "version": "4.1.0.Final"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-0e0bfa71-296c-4a0c-9e9c-f0e70ca44248",
          "value": "0e0bfa71-296c-4a0c-9e9c-f0e70ca44248",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/maven/io.netty/netty-all/CVE-2019-20444.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-20444",
          "value": "CVE-2019-20444",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-20444"
        }
      ],
      "cvss_vectors": [
        {
          "vendor": "NVD",
          "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:N"
        },
        {
          "vendor": "NVD",
          "vector": "AV:N/AC:L/Au:N/C:P/I:P/A:N"
        }
      ],
      "links": [
        {
          "url": "https://github.com/netty/netty/issues/9866"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-20444"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "io.netty/netty-all:4.1.0.Final"
        }
      }
    },
    {
      "id": "a8c126f1803becb291d726de934d8a0ba7646ecfd45278509cf64fa02ad56cbf",
      "name": "Inconsistent Interpretation of HTTP Requests (HTTP Request Smuggling)",
      "description": "`HttpObjectDecoder.java` in Netty allows a Content-Length header to be accompanied by a second `Content-Length` header, or by a `Transfer-Encoding` header.",
      "severity": "Critical",
      "solution": "Upgrade to version 4.1.44.Final or above.",
      "location": {
        "file": "build.gradle",
        "dependency": {
          "package": {
            "name": "io.netty/netty-all"
          },
          "version": "4.1.0.Final"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-4b617752-889e-481d-80a4-42276bd7985c",
          "value": "4b617752-889e-481d-80a4-42276bd7985c",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/maven/io.netty/netty-all/CVE-2019-20445.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-20445",
          "value": "CVE-2019-20445",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-20445"
        }
      ],
      "cvss_vectors": [
        {
          "vendor": "NVD",
          "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:N"
        },
        {
          "vendor": "NVD",
          "vector": "AV:N/AC:L/Au:N/C:P/I:P/A:N"
        }
      ],
      "links": [
        {
          "url": "https://github.com/netty/netty/issues/9861"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-20445"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "io.netty/netty-all:4.1.0.Final"
        }
      }
    },
    {
      "id": "3e88015057a3f4d16b3aff63f949107b00bc2d02dc34ee5cc462d2b42a867e4c",
      "name": "Improper Restriction of Operations within the Bounds of a Memory Buffer",
      "description": "The `ZlibDecoders` in Netty contains an unbounded memory allocation while decoding a `ZlibEncoded` byte stream. An attacker could send a large `ZlibEncoded` byte stream to the Netty server, forcing the server to allocate all of its free memory to a single decoder.",
      "severity": "Critical",
      "solution": "Upgrade to version 4.1.46.Final or above.",
      "location": {
        "file": "build.gradle",
        "dependency": {
          "package": {
            "name": "io.netty/netty-all"
          },
          "version": "4.1.0.Final"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-959c5b79-9095-4559-ba91-ed0d77284822",
          "value": "959c5b79-9095-4559-ba91-ed0d77284822",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/maven/io.netty/netty-all/CVE-2020-11612.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2020-11612",
          "value": "CVE-2020-11612",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-11612"
        }
      ],
      "cvss_vectors": [
        {
          "vendor": "NVD",
          "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H"
        },
        {
          "vendor": "NVD",
          "vector": "AV:N/AC:L/Au:N/C:P/I:P/A:P"
        }
      ],
      "links": [
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2020-11612"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "io.netty/netty-all:4.1.0.Final"
        }
      }
    },
    {
      "id": "106ab2b0fd367fccf35dfe594698b671010022081b742c6dccb432ad8363123b",
      "name": "Infinite Loop for SSL Handler",
      "description": "`handler/ssl/OpenSslEngine.java` in this package allows remote attackers to cause a denial of service (infinite loop).",
      "severity": "High",
      "solution": "Upgrade to the latest version",
      "location": {
        "file": "build.gradle",
        "dependency": {
          "package": {
            "name": "io.netty/netty-all"
          },
          "version": "4.1.0.Final"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-2760cba8-cfeb-4f93-9a8d-90efc3867570",
          "value": "2760cba8-cfeb-4f93-9a8d-90efc3867570",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/maven/io.netty/netty-all/CVE-2016-4970.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2016-4970",
          "value": "CVE-2016-4970",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-4970"
        }
      ],
      "cvss_vectors": [
        {
          "vendor": "NVD",
          "vector": "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H"
        },
        {
          "vendor": "NVD",
          "vector": "AV:N/AC:L/Au:N/C:N/I:N/A:C"
        }
      ],
      "links": [
        {
          "url": "http://netty.io/news/2016/06/07/4-0-37-Final.html"
        },
        {
          "url": "http://netty.io/news/2016/06/07/4-1-1-Final.html"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "io.netty/netty-all:4.1.0.Final"
        }
      }
    },
    {
      "id": "88b14005558d85d2790d2185acd9fe5989db8726d260d3efde9086d61bf3f0bb",
      "name": "Inconsistent Interpretation of HTTP Requests (HTTP Request Smuggling)",
      "description": "Netty mishandles whitespace before the colon in HTTP headers (such as a `Transfer-Encoding : chunked` line), which leads to HTTP request smuggling.",
      "severity": "High",
      "solution": "Upgrade to version 4.1.42.Final or above.",
      "location": {
        "file": "build.gradle",
        "dependency": {
          "package": {
            "name": "io.netty/netty-all"
          },
          "version": "4.1.0.Final"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-77bd807c-fbd2-4601-90d1-0a355f9c4d66",
          "value": "77bd807c-fbd2-4601-90d1-0a355f9c4d66",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/maven/io.netty/netty-all/CVE-2019-16869.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-16869",
          "value": "CVE-2019-16869",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-16869"
        }
      ],
      "cvss_vectors": [
        {
          "vendor": "NVD",
          "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:H/A:N"
        },
        {
          "vendor": "NVD",
          "vector": "AV:N/AC:L/Au:N/C:N/I:P/A:N"
        }
      ],
      "links": [
        {
          "url": "https://github.com/netty/netty/issues/9571"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-16869"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "io.netty/netty-all:4.1.0.Final"
        }
      }
    }
  ],
  "scan": {
    "analyzer": {
      "id": "gemnasium-maven",
      "name": "gemnasium-maven",
      "url": "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium",
      "vendor": {
        "name": "GitLab"
      },
      "version": "4.11.0"
    },
    "scanner": {
      "id": "gemnasium-maven",
      "name": "gemnasium-maven",
      "url": "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium",
      "vendor": {
        "name": "GitLab"
      },
      "version": "4.11.0"
    },
    "type": "dependency_scanning",
    "start_time": "2024-02-29T11:00:17",
    "end_time": "2024-02-29T11:00:29",
    "status": "success"
  }
}
