package parser

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

// Test_Registry is a combined tests where Register is tested indirectly.
func Test_Registry(t *testing.T) {
	// clear registry
	parsersMu.Lock()
	parsers = make(map[string]Parser)
	parsersMu.Unlock()

	// unsorted list of package types
	pkgTypes := []PackageType{PackageTypeNpm, PackageTypeMaven, PackageTypePackagist, PackageTypeGem, PackageTypeGo}

	// create two parsers for each package type, but with different filenames
	parsers := make(map[PackageType][]Parser)
	for _, pkgType := range pkgTypes {
		name := string(pkgType)
		parsers[pkgType] = []Parser{
			{
				// Parse function cannot be compared
				PackageType: pkgType,
				Filenames:   []string{name + " file"},
			},
			{
				// Parse function cannot be compared
				PackageType: pkgType,
				Filenames:   []string{"package." + name, "manifest." + name},
			},
		}
	}

	// register all parsers
	for _, pkgType := range pkgTypes {
		name := string(pkgType)
		for i, parser := range parsers[pkgType] {
			Register(fmt.Sprintf("%s%d", name, i+1), parser)
		}
	}

	t.Run("Lookup", func(t *testing.T) {
		t.Run("found", func(t *testing.T) {
			got := Lookup("manifest.npm")
			// last parser registered for that package type
			want := parsers[PackageTypeNpm][1]
			require.NotNil(t, got)
			require.Equal(t, &want, got)
		})
		t.Run("missing", func(t *testing.T) {
			p := Lookup("missing.npm")
			if p != nil {
				// can't use require.Nilf here, because testify tries to dereference p.PackageManager even
				// though p is nil - it must be pre-loading the string
				// TODO: file a bug report and find out if it's possible to lazily evaluate the format string
				t.Errorf("Expected no parser but got '%v'", p)
			}
		})
	})

	t.Run("Parsers", func(t *testing.T) {
		want := []string{"gem1", "gem2", "go1", "go2", "maven1", "maven2", "npm1", "npm2", "packagist1", "packagist2"}
		got := Parsers()
		require.ElementsMatch(t, want, got)
	})

	t.Run("PackageTypes", func(t *testing.T) {
		want := []string{"gem", "go", "maven", "npm", "packagist"}
		got := PackageTypes()
		require.ElementsMatch(t, want, got)
	})
}
