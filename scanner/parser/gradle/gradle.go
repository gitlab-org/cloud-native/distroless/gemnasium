package gradle

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
)

// Allocate the prefix and suffix bytes exactly once by declaring it at the top level.
var (
	reportPrefix = []byte("var projectDependencyReport =")
	reportSuffix = []byte(";")
)

// Declare the version separator string at the top level to ensure it is
// allocated exactly once. This separator is used in conflict resolution to
// distinguish between requested and selected versions in version strings.
const versionSeparator = " ➡ "

// Parse scans a Gradle htmlDependencyReport JS file and returns a list of packages
func Parse(r io.Reader, opts parser.Options) ([]parser.Package, []parser.Dependency, error) {
	input, err := io.ReadAll(r)
	if err != nil {
		return nil, nil, err
	}

	// The htmlDependencyReport generates a .js which binds some JSON to a variable
	input = bytes.TrimPrefix(input, reportPrefix)
	input = bytes.TrimSuffix(input, reportSuffix)

	var report Report
	err = json.Unmarshal(input, &report)
	if err != nil {
		return nil, nil, fmt.Errorf("gradle parser: parsing gradle dependencies report: %w", err)
	}

	pkgSet := make(map[pkg]bool)

	for _, config := range report.Project.Configurations {
		walk(pkgSet, config.Dependencies)
	}

	pkgs := make([]parser.Package, 0, len(pkgSet))
	for pkg := range pkgSet {
		pkgs = append(pkgs, parser.Package{Name: pkg.name(), Version: pkg.version})
	}

	return pkgs, nil, nil
}

// pkg represents the basic attributes of a package with its group, artifact,
// and version identifiers.
type pkg struct {
	group    string
	artifact string
	version  string
}

// name returns the concatenated group and artifact of the pkg as a single string.
func (p pkg) name() string {
	return p.group + "/" + p.artifact
}

// parsePkg parses a given package string in the "group:name:version"
// format.
//
// See http://archive.today/0qovR
func parsePkg(input string) (pkg, error) {
	pkgAttrs := strings.SplitN(input, ":", 3)

	if len(pkgAttrs) != 3 {
		return pkg{}, fmt.Errorf("expected group:name:version format, got: %s", input)
	}

	version := parseSelectedVersion(pkgAttrs[2])

	return pkg{group: pkgAttrs[0], artifact: pkgAttrs[1], version: version}, nil
}

// parseSelectedVersion extracts the selected version from a raw version string.
//
// If a package underwent conflict resolution, then the requested and selected
// versions are separated by a " ➡ " character, for example
// "<requested version> ➡ <selected version>".
//
// If the raw version contains a conflict resolution character " ➡ ", then the
// selected version is returned, otherwise the raw version is returned.
//
// Examples:
//
//	input: "1.7.25 ➡ 1.7.30"
//	output: "1.7.30"
//
//	input: "1.7.25"
//	output: "1.7.25"
//
// See https://docs.gradle.org/current/userguide/viewing_debugging_dependencies.html#example_rendering_the_dependency_report_for_a_custom_configuration for more details.
func parseSelectedVersion(rawVersion string) string {
	const selectedVersionIndex = 1

	requestedAndSelectedVersions := strings.SplitN(rawVersion, versionSeparator, 2)
	// the package underwent conflict resolution, return the selected version
	if len(requestedAndSelectedVersions) == 2 {
		return requestedAndSelectedVersions[selectedVersionIndex]
	}

	return rawVersion
}

// walk recursively processes a list of dependencies, adding each unique package
// to the set of packages. It only warns on parse errors because a malformed
// package may simply refer to a local dependency.
func walk(pkgSet map[pkg]bool, dependencies []Dependency) {
	for _, dep := range dependencies {
		pkg, err := parsePkg(dep.Name)
		if err != nil {
			log.Warnf("recoverable error encountered while parsing gradle package: %s", err)
		} else {
			pkgSet[pkg] = true
		}

		if len(dep.Children) > 0 {
			walk(pkgSet, dep.Children)
		}
	}
}

func init() {
	parser.Register("gradle", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypeMaven,
		Filenames:   []string{"gradle-html-dependency-report.js"},
	})
}
