package gradle

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/testutil"
)

func TestGradle(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		tcs := []struct {
			name            string
			fixtureName     string
			expectationName string
			options         parser.Options
		}{
			{
				"root.js",
				"multimodules/build/reports/project/dependencies",
				"root",
				parser.Options{},
			},
			{
				"root.api.js",
				"multimodules/api/build/reports/project/dependencies",
				"root-api",
				parser.Options{},
			},
			{
				"root.model.js",
				"multimodules/model/build/reports/project/dependencies",
				"root-model",
				parser.Options{},
			},
			{
				"root.web.js",
				"multimodules/web/build/reports/project/dependencies",
				"root-web",
				parser.Options{},
			},
			{
				"root.js",
				"conflict-resolution/build/reports/project/dependencies",
				"conflict-resolution",
				parser.Options{},
			},
		}

		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				fixture := testutil.Fixture(t, tc.fixtureName, tc.name)
				got, _, err := Parse(fixture, tc.options)
				require.NoError(t, err)
				testutil.RequireExpectedPackages(t, tc.expectationName, got)
			})
		}
	})
}
