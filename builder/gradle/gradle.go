package gradle

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/builder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/builder/exportpath"
)

const (
	pathDefaultGradleBinary = "/opt/asdf/shims/gradle"

	flagGradleOpts       = "gradle-opts"
	flagGradleInitScript = "gradle-init-script"
)

// Builder generates dependency lists for gradle projects
type Builder struct {
	GradleOpts          string
	GradleInitScript    string
	GradleTaskName      string
	GradlePathExtractor func([]byte) ([]string, error)
}

// Flags returns the CLI flags that configure the gradle command
func (b Builder) Flags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:    flagGradleOpts,
			Usage:   "Optional CLI arguments for the gradle dependency dump command",
			Value:   "",
			EnvVars: []string{"GRADLE_CLI_OPTS"},
		},
		&cli.StringFlag{
			Name:    flagGradleInitScript,
			Usage:   "Optional CLI argument pointing to the gradle init script",
			Value:   "gemnasium-init.gradle",
			EnvVars: []string{"GRADLE_PLUGIN_INIT_PATH"},
		},
	}
}

// Configure configures the gradle command
func (b *Builder) Configure(c *cli.Context) error {
	b.GradleOpts = c.String(flagGradleOpts)
	b.GradleInitScript = c.String(flagGradleInitScript)
	b.GradleTaskName = "htmlDependencyReport"
	b.GradlePathExtractor = exportpath.ExtractGradleHTMLDependencyReport

	return nil
}

// Build generates graph exports for all projects of a multi-project Gradle build.
// It returns the export path for the root project, and the export paths for its sub-projects, if any.
func (b Builder) Build(input string) (string, []string, error) {
	paths, err := b.listDeps(input)
	if err != nil {
		return "", []string{}, err
	}

	newPaths, err := normalizePaths(paths)
	if err != nil {
		return "", []string{}, fmt.Errorf("gradle builder: %w", err)
	}

	paths = newPaths

	return exportpath.Split(paths, filepath.Dir(input))
}

// listDeps exports the dependency list to JSON using the Gemnasium Gradle Plugin.
// It returns the paths to the JSON export created for the root project and its sub-projects, if any.
func (b Builder) listDeps(input string) ([]string, error) {
	// find gradle wrapper
	dir := filepath.Dir(input)
	gradlewPath := filepath.Join(dir, "gradlew")
	if _, err := os.Stat(gradlewPath); err != nil {
		gradlewPath = pathDefaultGradleBinary
	}

	// build arguments for dumping gradle dependencies
	args := strings.Fields(b.GradleOpts)
	args = append(args, "--init-script", b.GradleInitScript, b.GradleTaskName)
	cmd := exec.Command(gradlewPath, args...)
	cmd.Dir = dir
	cmd.Env = os.Environ()
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)
	if err != nil {
		return nil, err
	}

	return b.GradlePathExtractor(output)
}

// normalizePaths Relocates htmlDependencyReport artifacts so that they're adjacent to
// their respective build.gradle files for Gemnasium compatibility.
//
// Example 1:
//
// input: []string{"/app/build/reports/project/dependencies/root.js"}
// result: JS file moved to "/app/gradle-html-dependency-report.js"
//
// Example 2:
//
// input: []string{"/app/api/build/reports/project/dependencies/root.js"}
// result: JS file moved to "/app/api/gradle-html-dependency-report.js"
func normalizePaths(paths []string) ([]string, error) {
	var err error

	newPaths := make([]string, 0, len(paths))

	for _, oldPath := range paths {
		var newRootPath string

		newRootPath, err = deriveRootPath(oldPath)
		if err != nil {
			return newPaths, fmt.Errorf("failed to normalize %s: %w", oldPath, err)
		}

		newPath := filepath.Join(newRootPath, "gradle-html-dependency-report.js")
		if err = moveFile(oldPath, newPath); err != nil {
			return newPaths, fmt.Errorf("failed to normalize %s: %w", oldPath, err)
		}

		newPaths = append(newPaths, newPath)
	}

	return newPaths, nil
}

// deriveRootPath adjusts the report path to ensure Gemnasium compatibility,
// which expects build.gradle to reside in the same directory as the report. It
// achieves this by computing the root path for a given report file path by
// identifying the directory segment before 'build/reports/project/dependencies'.
//
// Example 1:
// input: "/app/build/reports/project/dependencies/root.js"
// result: "/app"
//
// Example 2:
// input: "/app/api/build/reports/project/dependencies/root.js"
// result: "/app/api"
//
// Example 3:
// input: "/app/api/build/reports/project/dependencies/root.api.js"
// result: "/app/api"
//
// Example 4:
// input: "/app/employees/build/reports/project/dependencies/root.js"
// result: "/app/employees"
//
// Example 5:
// input: "/app/handler/build/reports/project/dependencies/root.handler.js"
// result: "/app/handler"
func deriveRootPath(reportPath string) (string, error) {
	// The report is generated in the build/reports/project/dependencies directory by default
	// https://docs.gradle.org/current/javadoc/org/gradle/api/reporting/dependencies/HtmlDependencyReportTask.html
	before, _, found := strings.Cut(reportPath, filepath.Join("build", "reports", "project", "dependencies"))
	if !found {
		return "", fmt.Errorf("could not determine root path for report path: %s", reportPath)
	}

	rootPath := strings.TrimSuffix(before, string(filepath.Separator))

	return rootPath, nil
}

// moveFile attempts to move a file from a source path to a destination path.
func moveFile(src, dst string) error {
	if err := os.Rename(src, dst); err != nil {
		return fmt.Errorf("failed to move file from %s to %s: %w", src, dst, err)
	}

	return nil
}

func init() {
	builder.Register("gradle", &Builder{})
}
