package pipdeptree

import (
	"os"
	"os/exec"
)

func runPipdeptree(args []string, outputPath string) error {
	cmd := exec.Command("pipdeptree", args...)
	cmd.Stderr = os.Stderr

	f, err := os.Create(outputPath)
	if err != nil {
		return err
	}
	defer f.Close()
	cmd.Stdout = f

	if err := cmd.Start(); err != nil {
		return err
	}
	return cmd.Wait()
}

// CreateJSON saves the JSON output of pipdeptree into a file
func CreateJSON(path string) error {
	return runPipdeptree([]string{"--json", "--user"}, path)
}

// CreateJSONVirtualEnv saves the JSON output of pipdeptree into a file based on virtual environment
func CreateJSONVirtualEnv(path string, python string) error {
	//Excluding pip and setuptools as they are dependencies of the installer
	return runPipdeptree([]string{"--python", python, "-e", "pip,setuptools", "--json"}, path)
}
